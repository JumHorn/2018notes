# 20180601
#### 1. C++默认int
1. 数字默认是int类型   
所以1<<31会越界变成-2147483648

2. 函数参数传递int超出范围   
若函数是void func(int param);   
若传递参数func(-6442450944)超出范围会默认是-2147483647   

3. sizeof(),std的size()函数   
返回值是unsigned int

# 20180603
#### 1. qfiledialog默认文件名

QFileDialog::getSaveFileName(this,tr("title"),tr("filepath"),tr("filetype"))   
其中filepath如果是文件夹，则默认打开文件夹，如果filetype是文件全路径，则最后打开有文件默认名   

filetype起到文件过滤的作用，可以是字符串"Export File(*.csv)"


# 20180604
#### 1. git warning

Warning: Permanently added the RSA host key for IP address '13.250.177.223' to the list of known hosts.

解决方法   
git push -u origin master

# 20180606
#### 1. endl的<<数字括号

```C++
#include<iostream>
using namespace std;

int main()
{
	cout<< 2+3&4 << endl;
	cout<<"(2+3)&4:"<< (2+3)&4 << endl;
	cout<<"2+(3&4):"<< 2+(3&4) << endl;
}
```

error: reference to overloaded function could not be resolved;  
did you mean to call it?   
cout<< 2+3&4 << endl;   
需要加括号

#### 2. &运算优先级低于+

```C++
#include<iostream>
using namespace std;

int main()
{
	cout<< (2+3&4) << endl;
	cout<<"(2+3)&4:"<< ((2+3)&4) << endl;
	cout<<"2+(3&4):"<< (2+(3&4)) << endl;
}
```

# 20180608
#### 1. 浮点数表示
```C++
int main()
{
    double a = 12.;
    cout << a << endl;
}
```
#### 2. qt 调用button的click方法
当button为disable时这个，这个调用不起任何作用
可以选择直接使用调用槽函数

# 20180610
#### 1. 模板类分开定义
```C++
template<typename T>
//void heap::push(const T& t) //heap is not a class
void heap<T>::push(const T& t) //correct
{
    // out of bounds
    int index=end+1; //zero index reserved
    if(index>size+1)
    {
        return;
    }
    data[index]=t;
    while(index>1)
    {
        index=index/2;
        if(t>data[index])
        {
            data[2*index]=data[index];
            data[index]=t;
        }
        else
        {
            break;
        }
    }   
    end++;
}
```

#### 2. max_heap排序堆
```C++
/*
 * max heap implementation
 */
#include<iostream>
using namespace std;

template<typename T>
class heap
{
private:
    T *data;
    unsigned int size;
    unsigned int end;//the index of the last element
public:
    heap(unsigned int size=0);
    ~heap();
    
    void push(const T& t);
    T pop();
};

template<typename T>
heap<T>::heap(unsigned int size)
{
    data=new T[size+1]; 
    this->size=size+1;
    end=0;
}

template<typename T>
heap<T>::~heap()
{
    if(data!=NULL)
    {
        delete[] data;
    }
}

template<typename T>
void heap<T>::push(const T& t)
{
    // out of bounds
    int index=end+1; //zero index reserved
    if(index>size)
    {
        return;
    }
    data[index]=t;
    while(index>1)
    {
        if(t>data[index/2])
        {
            data[index]=data[index/2];
            data[index/2]=t;
        }
        else
        {
            break;
        }
        index=index/2;
    }   
    end++;
}

template<typename T>
T heap<T>::pop()
{
    // empty heap
    if(end==0)
    {
        return T();
    }
    data[0]=data[1];
    data[1]=data[end];
    int index=1;
    int maxnode=index*2+1;
    while(maxnode<=end)
    {
        if(data[maxnode]<data[maxnode-1])
            maxnode--;
        if(data[maxnode]>data[end])
        {
            data[maxnode/2]=data[maxnode];
            data[maxnode]=data[end];
        }
        else
        {
            break;
        }
        maxnode=2*maxnode+1;
    }
    //only left node
    if(maxnode-1==end&&data[maxnode-1]>data[maxnode/2])
    {
        data[maxnode/2]=data[maxnode];
        data[maxnode-1]=data[end];
    }
    end--;
    return data[0];
}
```

# 20180618
#### 1. moveCursor
qtextEdit光标移动，可以直接使用moveCursor方法。或者不使用moveCursor，在append方法下，cursor自动下移，当鼠标主动拖着滚动条向上时，cursor不会再下移

#### 2. 异步刷新显示界面
使用QTextEdit一边添加文本，一边显示，是不行的，显示的排在消息队列后面，所以用
QApplication::processEvent()先处理消息队列，再执行添加文本的功能，就可以实现异步显示
```C++
void Asynchronous::on_pushButton_clicked()
{
    QFile file("F:\\JumHorn\\text.txt");
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return;

    ui.plainTextEdit->clear();
    QTextStream in(&file);
    while(!in.atEnd())
    {
        QString line = in.readline();
        ui.plainTextEdit->appendPlainText(line);
        QApplication::processEvents();
    }

    ui.plainTextEdit->moveCursor(QTextCursor::Start);
}
```
这些想法可以在多线程的环境下实现
* 一种方法是在多线程下用信号和槽来实现，从文本中读取到一行便发送一个信号(新线程)，然后从(老线程)槽函数中将文本append到plainTextEdit后面。
这种实现方法基本没有什么用.如果信号的发送速度缓一缓(在发送中sleep一下)还是可以起到一点效果的
 
* 另外一种方法是将整个append事件放在线程中执行，但是我这里出现了QVector<T>::operator[]
out of bounds,说明QTextEdit不是线程安全的，无法使用多线程的方法

* 最后一种方法是使用QTableWidget，多行一列的方式来模拟文本，将读取到的文本加入到一个个单元格内，最后发现这个方法效率最高.但是这个方法失去了对原来文本的编辑功能

# 20180619
#### 1.windows下bat设置环境变量
环境变量设置区分系统环境变量和用户环境变量
```shell
@echo off
::这样设置环境变量要重启系统
::system environment variable
set regpath="HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Session Manager\Environment"
::用户环境变量是另一个路径HKEY_USERS\username\Environment
::用户名是一个复杂的数字，如果不知道可以在注册表中搜索一个已知的环境变量，便可以看到用户名
set evname=JAVA_HOME
set evpath="C:\program files"
reg add %regpath% /v %evname% /d %evpath% /f

::下面的方法可以临时设置环境变量,配合使用正好可以永久设置环境变量
setx evname evpath
```
环境变量是否设置成功，可以在命令行下输入echo %evname%查看

# 20180625
#### 1.编译单元头文件相互包含
```C++
//filename head1.h
#ifndef _HEAD1_H_
#define _HEAD1_H_
//#include "head2.h"  //未定义的标识符

struct MyStruct1
{

};
//在这个位置展开Mystruct1就在head2中提前定义了
#include "head2.h"  //编译通过

int fun1();

#endif

```

```C++
//filename head2.h
#ifndef _HEAD2_H_
#define _HEAD2_H_

#include "head1.h"

struct MyStruct2
{

};

int fun2(MyStruct1);

#endif
```

```C++
//filename head1.cpp
#include "head1.h"
```

```C++
//filename head2.cpp
#include "head2.h"
```

* 编译单元以单个cpp文件和所有cpp文件中的包含文件一起参与编译,在cpp文件中的include就在当前位置展开其所包含的文件,在c/c++中定义必须在调用之前

* 在相互包含头文件中，如果失败会出现未定义标识符的错误

* 所以头文件要尽量包含在.cpp文件中以减少依赖

# 20180627
#### 1.字符集问题
在UTF8中有些中文字符串还是会出现问题，代码如下
```C++
#include<iostream>
using namespace std;

int main()
{
	cout<<"以获得有关命令行的帮助"<<endl;
}
```

这类问题在编译时会出现一个不怎么引人注目的warning   
warning C4819: The file contains a character that cannot be represented in the current code page (936). Save the file in Unicode format to prevent data loss

一般情况下不会出现问题，但是也会出现以下error   
error C2001: newline in constant   
此时应该改变文件字符集   
可以在vs下file->advanced save options中选择UNICODE codepage 1200编码方式

# 20180628
#### 1.设置静态链接C运行库
项目->属性->C/C++->代码生成->运行库 将 多线程DLL(/MDd) 或者多线程调试DLL(/MTd) 改为 多线程(/MD) 或者 多线程调试(/MT)   
多线程(/MD) 和 多线程调试(/MT) 的区别就是Release和Debug的区别

# 20180630
#### 1. use of member as default parameter requires static member
```C++
class Dog
{
public:
    int height;
    int get(int h=height)
    {}
};
```