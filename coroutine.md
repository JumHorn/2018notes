# 协程
主要思想是在一个线程中实现异步事件。相当于在函数中间加中断，将cpu让给其他函数执行，下次再执行该函数时从该中断处开始执行

# C语言实现协程
一下是生产者消费者例子，不用任何加锁，实现异步事件
```C++
/*

Coroutines in C

based on this trick,
so the following macro crBegin,crReturn,crFinish are the approach

void DuffsDevice()
{
    int count=53;
    int i=1,j=1;
    switch(count%8)
    {
    case 0: do{ i=j++;
    case 7:     i=j++;
    case 6:     i=j++;
    case 5:     i=j++;
    case 4:     i=j++;
    case 3:     i=j++;
    case 2:     i=j++;
    case 1:     i=j++;
            }while(count-->0);
    }
}

there is a consumer and producer example to illustrate this idea
*/

#include<iostream>
using namespace std;

#define crBegin static int state=0; switch(state) { case 0:
#define crReturn(x) do { state=__LINE__; return x; \
                         case __LINE__:; } while (0)
#define crFinish }

//the shared buffer
#define SIZE 8
char buffer[8];
int freed;
int used;

//producer function
void produce()
{
    crBegin;
    while(1)
    {
        buffer[used]="coroutin"[used];
        cout<<"producer input "<<"coroutin"[used]<<endl;
        used++;
        used&=SIZE-1;
        crReturn( );//yeild to consume
    }
    crFinish;
}

//consumer function
void consume()
{
    crBegin;
    while(1)
    {
        cout<<"consumer output "<<buffer[freed]<<endl;
        freed++;
        freed&=SIZE-1;
        crReturn( );//yeild to produce
    }
    crFinish;
}

int main()
{
    while(1)
    {
        char c=getchar();
        produce();
        consume();
    }
    return 0;
}

```

# 其他库
Boost库和glibc库中都实现了协程