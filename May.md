# 20180521

#### 1. 无参数构造函数

不能作为类的成员，不能放入容器，但是可以使用指针

#### 2. QString转char*
* 方法1

先通过toLocal8Bit()转化为QByteArray,   
然后再调用data()将QByteArray转化为char*      
这两个要分开，不能在一步中完成   


* 方法2

通过toStdString()将它转化为string，然后再调用c_str()方法转化为char*   
可以在一步中完成


#### 3. sizeof(string)

```C++
#include<iostream>
#include<string>
using namespace std;

int main()
{
	string a = "中文长度";
	cout << a << endl;
	cout << a.length() << endl;
	cout << sizeof(a) << endl;
	return 0;
}
```
sizeof(string)是string类的固定长度是24，是各个成员组成的总长度

#### 4. 调试的行不准

* 代码没有生成成功，使用的是之前版本的代码   
在vs下勾选了运行上次成功的案例导致   
可以更改设置,在工具->选项->项目和解决方案->生成和运行->当生成和部署错误时

* 没有载入pdb调试信息

* release模式下的调试
代码运行不准，也是勾选了，vs下的设置   
工具->选项->调试->常规->要求源文件与原始版本完全匹配   
release代码早就经过优化，所以才导致和源码对不上

#### 5. 中文字符串char*长度计算

```C++
#include<iostream>
#include<string>
using namespace std;

int main()
{
	string a = "中文长度";
	cout << a << endl;
	cout << a.length() << endl;
	cout << sizeof(a) << endl;
	cout << strlen(a.c_str()) << endl;
	return 0;
}
```

# 20180522
#### QList
* array of pointers
* never shrink only deallocated by the destructor or by the assignment operator
* at() can be faster than operator[]() because it never cause deep copy

#### strlen
strlen不包含\0   
中文长度在windows下是两个字节，即认为是宽字节，   程序在读取第一个字节时，发现不是ascii码，就认为是款字节，   
即两个字节表示一个字   
在Mac下是一个字节

#### 符号文件 
vs符号文件pdb信息



